package com.hlkj.sellergoodsservicealiyun.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.pojo.model.Brand;
import com.hlkj.sellergoodsservicealiyun.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    @GetMapping("/findAll")
    public List<Brand> findAll() {
        return brandService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult findPage(Integer pageNo, Integer pageSize) {
        return  brandService.findPage(pageNo, pageSize );
    }
}
