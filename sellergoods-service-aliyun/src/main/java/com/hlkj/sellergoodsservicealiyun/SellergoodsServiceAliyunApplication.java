package com.hlkj.sellergoodsservicealiyun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SellergoodsServiceAliyunApplication {

    public static void main(String[] args) {
        SpringApplication.run(SellergoodsServiceAliyunApplication.class, args);
    }

}
