package com.hlkj.sellergoodsservicealiyun.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hlkj.common.common.PageResult;
import com.hlkj.pojo.model.Brand;
import com.hlkj.sellergoodsservicealiyun.mapper.BrandMapper;
import com.hlkj.sellergoodsservicealiyun.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public List<Brand> findAll() {
        return brandMapper.selectByExample(null);
    }

    @Override
    public PageResult findPage(Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        Page<Brand> page = (Page<Brand>)brandMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }
}
