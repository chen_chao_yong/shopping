package com.hlkj.sellergoodsservicealiyun.service;

import com.hlkj.common.common.PageResult;
import com.hlkj.pojo.model.Brand;

import java.util.List;

public interface BrandService {

    List<Brand> findAll();

    PageResult findPage(Integer pageNo, Integer pageSize);
}
