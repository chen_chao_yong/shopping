package com.hlkj.sellergoodsservicealiyun;

import com.hlkj.sellergoodsservicealiyun.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SellergoodsServiceAliyunApplicationTests {

    @Autowired
    private BrandService brandService;

    @Test
    void contextLoads() {
        System.out.println(brandService.findAll());
    }

}
