package com.hlkj.managerweb.feign;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Item;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("sellergoods-service")
public interface ItemFeignClient {

    @GetMapping("/item/findPage")
    public PageResult findPage(@RequestParam("pageNo") Integer pageNo,
                               @RequestParam("pageSize") Integer PageSize);

    @PostMapping("/item/add")
    public Result add(Item item);

    @PostMapping("/item/update")
    public Result update(Item item);

    @GetMapping("/item/findOne")
    public Item findOne(@RequestParam("id") Long id);

    @PostMapping("/item/search")
    public PageResult search(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             Item item);

    @GetMapping("/item/delete")
    public Result delete(@RequestParam("ids") Long[] ids);

}
