package com.hlkj.managerweb.feign;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.TypeTemplate;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("sellergoods-service")
public interface TypeTemplateFeignClient {

    @GetMapping("/typeTemplate/findPage")
    public PageResult findPage(@RequestParam("pageNo") Integer pageNo,
                               @RequestParam("pageSize") Integer PageSize);

    @PostMapping("/typeTemplate/add")
    public Result add(TypeTemplate record);

    @PostMapping("/typeTemplate/update")
    public Result update(TypeTemplate record);

    @GetMapping("/typeTemplate/findOne")
    public TypeTemplate findOne(@RequestParam("id") Long id);

    @PostMapping("/typeTemplate/search")
    public PageResult search(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             TypeTemplate record);

    @GetMapping("/typeTemplate/delete")
    public Result delete(@RequestParam("ids") Long[] ids);
}
