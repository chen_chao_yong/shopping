package com.hlkj.managerweb.feign;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.ItemCat;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("sellergoods-service")
public interface ItemCatFeignClient {

    @GetMapping("/itemCat/findPage")
    public PageResult findPage(@RequestParam("pageNo") Integer pageNo,
                               @RequestParam("pageSize") Integer PageSize);

    @PostMapping("/itemCat/add")
    public Result add(ItemCat itemCat);

    @PostMapping("/itemCat/update")
    public Result update(ItemCat itemCat);

    @GetMapping("/itemCat/findOne")
    public ItemCat findOne(@RequestParam("id") Long id);

    @PostMapping("/itemCat/search")
    public PageResult search(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             ItemCat itemCat);

    @GetMapping("/itemCat/delete")
    public Result delete(@RequestParam("ids") Long[] ids);

}
