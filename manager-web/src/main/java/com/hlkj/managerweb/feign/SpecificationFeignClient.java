package com.hlkj.managerweb.feign;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Specification;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient("sellergoods-service")
public interface SpecificationFeignClient {

    @GetMapping("/specification/findPage")
    public PageResult findPage(@RequestParam("pageNo") Integer pageNo,
                               @RequestParam("pageSize") Integer PageSize);

    @PostMapping("/specification/add")
    public Result add(Specification record);

    @PostMapping("/specification/update")
    public Result update(Specification record);

    @GetMapping("/specification/findOne")
    public Specification findOne(@RequestParam("id") Long id);

    @PostMapping("/specification/search")
    public PageResult search(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             Specification record);

    @GetMapping("/specification/delete")
    public Result delete(@RequestParam("ids") Long[] ids);

    @GetMapping("/specification/selectOptionList")
    List<Map> seleteOptionList();
}
