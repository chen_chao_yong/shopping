package com.hlkj.managerweb.feign;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Brand;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient("sellergoods-service")
public interface BrandFeignClient {

    @GetMapping("/brand/findPage")
    public PageResult findPage(@RequestParam("pageNo") Integer pageNo,
                               @RequestParam("pageSize") Integer PageSize);

    @PostMapping("/brand/add")
    public Result add(Brand brand);

    @PostMapping("/brand/update")
    public Result update(Brand brand);

    @GetMapping("/brand/findOne")
    public Brand findOne(@RequestParam("id") Long id);

    @PostMapping("/brand/search")
    public PageResult search(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             Brand brand);

    @GetMapping("/brand/delete")
    public Result delete(@RequestParam("ids") Long[] ids);

    @GetMapping("/brand/selectOptionList")
    public List<Map> selectOptionList();
}
