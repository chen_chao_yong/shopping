package com.hlkj.managerweb.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.managerweb.feign.SpecificationFeignClient;
import com.hlkj.pojo.model.Specification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/specification")
public class SpecificationController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SpecificationFeignClient specificationFeignClient;

    @RequestMapping("/findAll")
    public List<Specification> findAll() {
        String url = "http://sellergoods-service/specification/findAll";
        return restTemplate.getForObject(url, List.class);
    }

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestParam("pageNo")Integer pageNo,
                               @RequestParam("pageSize") Integer pageSize){
        return specificationFeignClient.findPage(pageNo, pageSize);
    }

    @PostMapping("/add")
    public Result add(@RequestBody Specification specification) {
        Result ret = specificationFeignClient.add(specification);
        return ret;
    }

    @PostMapping("/update")
    public Result update(@RequestBody Specification specification) {
        System.out.println(specification);
        Result ret = specificationFeignClient.update(specification);
        return ret;
    }

    @GetMapping("/findOne")
    public Specification findOne(Long id) {
        Specification specification = specificationFeignClient.findOne(id);
        return specification;
    }

    @PostMapping("/search")
    public PageResult search(@RequestParam("page") Integer pageNo,
                             @RequestParam("rows") Integer pageSize,
                             @RequestBody Specification specification) {
        System.out.println(pageNo+"-----"+pageSize+"------"+specification.getSpecName());
        return specificationFeignClient.search(pageNo, pageSize, specification);
    }

    @RequestMapping("/delete")
    public Result delete(Long[] ids) {
        for(Long id : ids) {
            System.out.println(id);
        }
        return specificationFeignClient.delete(ids);
    }

    @RequestMapping("/selectOptionList")
    public List<Map> seleteOptionList(){
        return specificationFeignClient.seleteOptionList();
    }
}
