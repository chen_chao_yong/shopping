package com.hlkj.managerweb.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.managerweb.feign.TypeTemplateFeignClient;
import com.hlkj.pojo.model.TypeTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/typeTemplate")
public class TypeTemplateController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private TypeTemplateFeignClient typeTemplateFeignClient;

    @RequestMapping("/findAll")
    public List<TypeTemplate> findAll() {
        String url = "http://sellergoods-service/typeTemplate/findAll";
        return restTemplate.getForObject(url, List.class);
    }

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestParam("pageNo")Integer pageNo,
                               @RequestParam("pageSize") Integer pageSize){
        return typeTemplateFeignClient.findPage(pageNo, pageSize);
    }

    @PostMapping("/add")
    public Result add(@RequestBody TypeTemplate typeTemplate) {
        Result ret = typeTemplateFeignClient.add(typeTemplate);
        return ret;
    }

    @PostMapping("/update")
    public Result update(@RequestBody TypeTemplate typeTemplate) {
        System.out.println(typeTemplate);
        Result ret = typeTemplateFeignClient.update(typeTemplate);
        return ret;
    }

    @GetMapping("/findOne")
    public TypeTemplate findOne(Long id) {
        TypeTemplate typeTemplate = typeTemplateFeignClient.findOne(id);
        return typeTemplate;
    }

    @PostMapping("/search")
    public PageResult search(@RequestParam("page") Integer pageNo,
                             @RequestParam("rows") Integer pageSize,
                             @RequestBody TypeTemplate typeTemplate) {
        System.out.println(pageNo+"-----"+pageSize+"------"+typeTemplate.getName());
        return typeTemplateFeignClient.search(pageNo, pageSize, typeTemplate);
    }

    @RequestMapping("/delete")
    public Result delete(Long[] ids) {
        for(Long id : ids) {
            System.out.println(id);
        }
        return typeTemplateFeignClient.delete(ids);
    }
}
