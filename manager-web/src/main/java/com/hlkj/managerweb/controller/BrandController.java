package com.hlkj.managerweb.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.managerweb.feign.BrandFeignClient;
import com.hlkj.pojo.model.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BrandFeignClient brandFeignClient;

    @RequestMapping("/findAll")
    public List<Brand> findAll() {
        String url = "http://sellergoods-service/brand/findAll";
        return restTemplate.getForObject(url, List.class);
    }

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestParam("pageNo")Integer pageNo,
                               @RequestParam("pageSize") Integer pageSize){
        return brandFeignClient.findPage(pageNo, pageSize);
    }

    @PostMapping("/add")
    public Result add(@RequestBody Brand brand) {
        Result ret = brandFeignClient.add(brand);
        return ret;
    }

    @PostMapping("/update")
    public Result update(@RequestBody Brand brand) {
        Result ret = brandFeignClient.update(brand);
        return ret;
    }

    @GetMapping("/findOne")
    public Brand findOne(Long id) {
        Brand brand = brandFeignClient.findOne(id);
        return brand;
    }

    @PostMapping("/search")
    public PageResult search(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             @RequestBody Brand brand) {
        System.out.println(pageNo+"----"+pageSize+"----"+brand.getName());
        return brandFeignClient.search(pageNo, pageSize, brand);
    }

    @RequestMapping("/delete")
    public Result delete(Long[] ids) {
        return brandFeignClient.delete(ids);
    }

    @RequestMapping("/selectOptionList")
    public List<Map> selectOptionList(){
        return brandFeignClient.selectOptionList();
    }
}
