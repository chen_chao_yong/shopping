package com.hlkj.managerweb.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.managerweb.feign.ItemCatFeignClient;
import com.hlkj.pojo.model.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/itemCat")
public class ItemCatController {

    @Autowired
    private ItemCatFeignClient itemCatFeignClient;

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestParam("page") Integer page,
                               @RequestParam("rows") Integer rows) {
        return itemCatFeignClient.findPage(page, rows);
    }

    @PostMapping("/add")
    public Result add(@RequestBody ItemCat itemCat) {
        Result ret = itemCatFeignClient.add(itemCat);
        return ret;
    }

    @PostMapping("/update")
    public Result update(@RequestBody ItemCat itemCat) {
        Result ret = itemCatFeignClient.update(itemCat);
        return ret;
    }

    @GetMapping("/findOne")
    public ItemCat findOne(Long id) {
        ItemCat itemCat = itemCatFeignClient.findOne(id);
        return itemCat;
    }

    @PostMapping("/search")
    public PageResult search(@RequestParam("page") Integer page,
                             @RequestParam("rows") Integer rows,
                             @RequestBody ItemCat itemCat) {
        System.out.println(page + "----" + rows + "----" + itemCat.getName());
        return itemCatFeignClient.search(page, rows, itemCat);
    }

    @RequestMapping("/delete")
    public Result delete(Long[] ids) {
        return itemCatFeignClient.delete(ids);
    }
}
