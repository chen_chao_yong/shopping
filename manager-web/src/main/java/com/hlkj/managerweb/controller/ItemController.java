package com.hlkj.managerweb.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.managerweb.feign.ItemFeignClient;
import com.hlkj.pojo.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/item")
public class ItemController {

    @Autowired
    private ItemFeignClient itemFeignClient;

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestParam("page") Integer page,
                               @RequestParam("rows") Integer rows) {
        return itemFeignClient.findPage(page, rows);
    }

    @PostMapping("/add")
    public Result add(@RequestBody Item item) {
        Result ret = itemFeignClient.add(item);
        return ret;
    }

    @PostMapping("/update")
    public Result update(@RequestBody Item item) {
        Result ret = itemFeignClient.update(item);
        return ret;
    }

    @GetMapping("/findOne")
    public Item findOne(Long id) {
        Item item = itemFeignClient.findOne(id);
        return item;
    }

    @PostMapping("/search")
    public PageResult search(@RequestParam("page") Integer page,
                             @RequestParam("rows") Integer rows,
                             @RequestBody Item item) {
        System.out.println(page + "----" + rows + "----" + item.getTitle());
        return itemFeignClient.search(page, rows, item);
    }

    @RequestMapping("/delete")
    public Result delete(Long[] ids) {
        return itemFeignClient.delete(ids);
    }
}
