package com.hlkj.redisservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Set;

@SpringBootTest
class RedisServiceApplicationTests {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void contextLoads() {
//        redisTemplate.boundValueOps("user10").set("chao");
//        System.out.println(redisTemplate.boundValueOps("user10").get());
//        redisTemplate.boundValueOps("nameset").set("曹贼");
//        redisTemplate.boundValueOps("nameset").set("刘备");
//        redisTemplate.boundValueOps("nameset").set("孙权");
        Set nameset = redisTemplate.boundSetOps("nameset").members();
        System.out.println(nameset);
    }

}
