package com.hlkj.redisservice.service.impl;

import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Content;
import com.hlkj.redisservice.service.RedisListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RedisListServiceImpl implements RedisListService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Result save(String key, List<Content> contents) {
        try{
            contents.forEach(e -> {
                redisTemplate.boundListOps(key).leftPush(e);
            });
            return new Result(true, "写入redis");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "写入失败");
        }
    }

    @Override
    public List<Object> find(String key) {
        List list = redisTemplate.boundListOps(key).range(0, redisTemplate.boundListOps(key).size());
        return list;
    }
}
