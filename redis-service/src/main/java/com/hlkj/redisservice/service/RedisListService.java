package com.hlkj.redisservice.service;

import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Content;

import java.util.List;

public interface RedisListService {

    public Result save(String name, List<Content> contents);
    public List<Object> find(String key);
}
