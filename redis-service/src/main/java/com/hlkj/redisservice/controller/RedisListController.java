package com.hlkj.redisservice.controller;

import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Content;
import com.hlkj.redisservice.service.RedisListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/list")
public class RedisListController {

    @Autowired
    private RedisListService redisListService;

    @PostMapping("/save")
    public Result save(@RequestParam("key") String key, @RequestBody List<Content> contents) {
        return redisListService.save(key, contents);
    }

    @GetMapping("/find")
    public List find(@RequestParam("key") String key) {
        return redisListService.find(key);
    }
}
