package com.hlkj.portalweb.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 不能让其它的包中也有安全，我们加入UserDetail的信息
 */
public class User extends com.hlkj.pojo.sys.User implements UserDetails {

    private List<GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<GrantedAuthority> authorities) {

        this.authorities = authorities;
    }

    public void setAuthorities() {
        if (this.getPermissionList() != null && this.getPermissionList().size() > 0) {
            List<GrantedAuthority> auths = new ArrayList<>();
            this.getPermissionList().stream().forEach(e -> {
                auths.add(new SimpleGrantedAuthority(e.getPermTag()));
            });
            this.authorities = auths;
        }
    }
}
