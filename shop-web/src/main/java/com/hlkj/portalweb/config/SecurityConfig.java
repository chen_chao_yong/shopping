package com.hlkj.portalweb.config;

import com.hlkj.pojo.sys.Permission;
import com.hlkj.portalweb.handler.MyAuthenticationFailuseHandler;
import com.hlkj.portalweb.handler.MyAuthenticationSuccessHandler;
import com.hlkj.portalweb.service.MyUserDetailsService;
import com.hlkj.portalweb.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    //用户
    @Autowired
    private MyUserDetailsService myUserDetailsService;
    //权限
    @Autowired
    private PermissionService permissionService;
    //验证成功
    @Autowired
    private MyAuthenticationSuccessHandler successHandler;
    //验证失败
    @Autowired
    private MyAuthenticationFailuseHandler failuseHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //密码验证
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        auth.userDetailsService(myUserDetailsService).passwordEncoder(new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                //加密过程
                return bCryptPasswordEncoder.encode(charSequence);
//                return MD5Util.encode(charSequence.toString());
            }

            //加密的密码与数据库密码进行比对
            //rawPassword 表单密码   encodePassword数据库密码
            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return bCryptPasswordEncoder.matches(rawPassword,encodedPassword);
            }
        });
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests = http.authorizeRequests();
        List<Permission> permissions = permissionService.findAllPermission();
        if(permissions!=null&&permissions.size()>0){
            permissions.stream().forEach(e->{
                authorizeRequests.antMatchers(e.getUrl()).hasAnyAuthority(e.getPermTag());
            });
        }
        authorizeRequests.antMatchers("/login").permitAll()
                .antMatchers("/css/**","/img/**","/js/**","/plugin/**").permitAll()
                .antMatchers("/**").fullyAuthenticated()
                .and().headers().frameOptions().disable()
                .and().formLogin().loginPage("/login")
                .successHandler(successHandler).failureHandler(failuseHandler)
                .and().csrf().disable();
    }
}
