package com.hlkj.portalweb.service;

import com.hlkj.pojo.sys.Permission;
import com.hlkj.portalweb.fegin.PermissionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionService {

    @Autowired
    private PermissionFeignClient permissionFeignClient;

    public List<Permission> findAllPermission(){
        return permissionFeignClient.findAllPermission();
    }
}
