package com.hlkj.portalweb.service;

import com.hlkj.pojo.sys.Permission;
import com.hlkj.portalweb.fegin.UserFeignClient;
import com.hlkj.portalweb.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserFeignClient userFeginClient;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userFeginClient.findUserByUserName(username);
        System.out.println(user);
        if(!Objects.isNull(user)) {
            List<Permission> permissions = userFeginClient.findPermissionsByUserName(username);
            if(permissions!=null&&permissions.size()>0){
                List<GrantedAuthority> authorities = new ArrayList<>();
                permissions.stream().forEach(e->{
                    authorities.add(new SimpleGrantedAuthority(e.getPermTag()));
                });
                user.setAuthorities(authorities);
            }
            return user;
        }else {
            return null;
        }
    }
}
