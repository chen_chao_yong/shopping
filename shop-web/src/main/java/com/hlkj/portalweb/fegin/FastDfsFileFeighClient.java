package com.hlkj.portalweb.fegin;

import com.hlkj.pojo.file.FastDfsFile;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

@FeignClient("fastdfs-service")
public interface FastDfsFileFeighClient {

    @PostMapping(value = "/fastdfs/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public FastDfsFile upload(@RequestPart(value = "file", required = false) MultipartFile file);

    @GetMapping(value="/fastdfs/download")
    public byte[] download(Long id);
}
