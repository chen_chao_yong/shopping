package com.hlkj.portalweb.fegin;

import com.hlkj.pojo.sys.Permission;
import com.hlkj.portalweb.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("seller-user-service")
public interface UserFeignClient {

    @GetMapping("/user/findUserByUserName")
    public User findUserByUserName(@RequestParam("username") String username);

    @GetMapping("/user/findPermissionsByUserName")
    public List<Permission> findPermissionsByUserName(@RequestParam("username") String username);
}
