package com.hlkj.portalweb.fegin;

import com.hlkj.common.common.PageResult;
import com.hlkj.pojo.model.Goods;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("sellergoods-service")
public interface GoodsFeignClient {

    @PostMapping("/goods/search")
    public PageResult search(@RequestParam("page") Integer page, @RequestParam("rows") Integer rows, @RequestBody Goods goods);

    @GetMapping("/goods/findOne")
    public Goods findOne(@RequestParam("id") Long id);
}
