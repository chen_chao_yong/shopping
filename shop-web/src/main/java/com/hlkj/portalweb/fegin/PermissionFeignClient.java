package com.hlkj.portalweb.fegin;

import com.hlkj.pojo.sys.Permission;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("seller-user-service")
public interface PermissionFeignClient {

    @GetMapping("/permission/findAllPermissions")
    public List<Permission> findAllPermission();
}
