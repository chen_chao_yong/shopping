package com.hlkj.portalweb.fegin;

import com.hlkj.pojo.model.ItemCat;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("sellergoods-service")
public interface ItemCatFeignClient {

    @GetMapping("/itemCat/searchItemCatByParentId")
    public List<ItemCat> searchItemCatByParentId(@RequestParam("parentId") Long parentId);

}
