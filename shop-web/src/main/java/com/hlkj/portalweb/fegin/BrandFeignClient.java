package com.hlkj.portalweb.fegin;

import com.hlkj.pojo.model.Brand;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("sellergoods-service")
public interface BrandFeignClient {

    @GetMapping("/brand/findAll")
    public List<Brand> findAll();
}
