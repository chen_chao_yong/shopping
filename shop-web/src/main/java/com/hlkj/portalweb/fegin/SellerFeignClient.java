package com.hlkj.portalweb.fegin;

import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Seller;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("sellergoods-service")
public interface SellerFeignClient {

    @PostMapping("/seller/add")
    public Result add(Seller seller);
}
