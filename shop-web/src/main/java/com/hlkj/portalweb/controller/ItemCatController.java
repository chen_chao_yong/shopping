package com.hlkj.portalweb.controller;

import com.hlkj.pojo.model.ItemCat;
import com.hlkj.portalweb.fegin.ItemCatFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/itemCat")
public class ItemCatController {

    @Autowired
    private ItemCatFeignClient itemCatFeignClient;

    @RequestMapping("searchItemCatByParentId")
    public List<ItemCat> searchItemCatByParentId(Long parentId) {
        return itemCatFeignClient.searchItemCatByParentId(parentId);
    }
}
