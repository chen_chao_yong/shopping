package com.hlkj.portalweb.controller;

import com.hlkj.common.common.Result;
import com.hlkj.pojo.file.FastDfsFile;
import com.hlkj.portalweb.fegin.FastDfsFileFeighClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;

@RestController
@RequestMapping("/fastdfs")
public class FastDfsFileController {

    @Autowired
    private FastDfsFileFeighClient fastDfsFileFeighClient;

    @PostMapping("/upload")
    public FastDfsFile upload(@RequestPart(value = "file", required = false) MultipartFile file) {
        return fastDfsFileFeighClient.upload(file);
    }

    @RequestMapping("/download/{id}")
    public Result download(@PathVariable Long id, HttpServletRequest req, HttpServletResponse res) {
        Result result = null;
        byte[] bytes = null;
        try{
            bytes = fastDfsFileFeighClient.download(id);
            res.setHeader("Content-Disposition", "attachment;Filename=" + URLEncoder.encode("aaa", "UTF-8"));
            OutputStream output = res.getOutputStream();
            output.write(bytes);
            output.flush();
            output.close();
            result = new Result(true, "下载失败");
        }catch (Exception e) {
            e.printStackTrace();
            result = new Result(false, "下载失败!");
        }
        return result;
    }
}
