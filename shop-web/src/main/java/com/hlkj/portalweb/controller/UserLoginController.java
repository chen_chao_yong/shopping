package com.hlkj.portalweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserLoginController {

    @RequestMapping("/login")
    public String login() {
        return "shoplogin";
    }

    @RequestMapping("/index")
    public String index() {
        return "/admin/index";
    }
}
