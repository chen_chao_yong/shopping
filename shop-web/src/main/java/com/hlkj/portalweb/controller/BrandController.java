package com.hlkj.portalweb.controller;

import com.hlkj.pojo.model.Brand;
import com.hlkj.portalweb.fegin.BrandFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private BrandFeignClient brandFeignClient;

    @RequestMapping("findAll")
    public List<Brand> findAll(){
        return brandFeignClient.findAll();
    }
}
