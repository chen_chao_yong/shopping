package com.hlkj.portalweb.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Goods;
import com.hlkj.portalweb.fegin.GoodsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsFeignClient goodsFeignClient;

    @PostMapping("/search")
    public PageResult search(Integer page, Integer rows, Goods goods) {
        return goodsFeignClient.search(page, rows, goods);
    }

    @PostMapping("/add")
    public Result add(@RequestBody Goods goods) {
        Result ret = new Result(true,"保存成功!");
        System.out.println("goods.goods:"+goods.getGoods().getBrandId()+"---"+goods.getGoods().getCategory1Id());
        System.out.println("goods.goodsDesc.introduction:"+goods.getGoodsDesc().getIntroduction());
        System.out.println("goods.goodsDesc.packageList:"+goods.getGoodsDesc().getPackageList());
        System.out.println("goods.goodsDesc.saleService:"+goods.getGoodsDesc().getSaleService());
        return ret;
    }

    @GetMapping("/findOne")
    public Goods findOne(Long id) {
        return goodsFeignClient.findOne(id);
    }
}
