//提交数据到服务器 上的接口
app.service('brandService', function ($http) {

    this.search=function (pageNo, pageSize, entity) {
        return $http.post("/brand/search?pageNo="+pageNo+"&pageSize="+pageSize, entity);
    }
    this.findAll=function () {
        return $http.get("/brand/findAll");
    }
    this.add=function (entity) {
        return $http.post("/brand/add", entity);
    }
    this.update=function (entity) {
        return $http.post("/brand/update", entity);
    }
    this.findOne=function (id) {
        return $http.get("/brand/findOne?id="+id);
    }
    this.findPage=function (pageNo, pageSize) {
        return $http.get("/brand/findPage?pageNo="+pageNo+"&pageSize="+pageSize);
    }
    this.dele=function (ids) {
        return $http.get("/brand/delete?ids="+ids);
    }
    //下拉表数据
    this.selectOptionList=function () {
        return $http.get("/brand/selectOptionList?");
    }
})