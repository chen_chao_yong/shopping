var app=angular.module("shopping", []);
app.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.html5Mode({
        enable: true,
        requireBase: false
    });
}]);