package com.hlkj.managerwebaliyun.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.managerwebaliyun.feign.BrandFeignClient;
import com.hlkj.pojo.model.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BrandFeignClient brandFeignClient;

    @RequestMapping("/findAll")
    public List<Brand> findAll() {
        String url = "http://sellergoods-service-aliyun/brand/findAll";
        return restTemplate.getForObject(url, List.class);
    }

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestParam("pageNo") Integer pageNo,
                               @RequestParam("pageSize") Integer pageSize) {
        return brandFeignClient.findPage(pageNo, pageSize);
    }
}
