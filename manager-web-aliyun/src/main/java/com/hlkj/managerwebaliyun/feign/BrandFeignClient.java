package com.hlkj.managerwebaliyun.feign;

import com.hlkj.common.common.PageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("sellergoods-service-aliyun")
public interface BrandFeignClient {

    @GetMapping("/brand/findPage")
    public PageResult findPage(@RequestParam("pageNo") Integer pageNo,
                               @RequestParam("pageSize") Integer pageSize);
}
