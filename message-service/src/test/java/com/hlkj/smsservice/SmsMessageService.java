package com.hlkj.smsservice;

import com.hlkj.smsservice.model.Message;
import com.hlkj.smsservice.service.MessageService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SmsMessageService {

    @Autowired
    private MessageService messageService;

    @Test
    void contextLoads() {
        Message message = new Message();
        message.setPhone("18820516850");
        System.out.println(messageService.sendMessage(message));
    }
}
