package com.hlkj.smsservice.service.impl;

import com.hlkj.smsservice.config.MessageUser;
import com.hlkj.smsservice.model.Message;
import com.hlkj.smsservice.model.MessageResult;
import com.hlkj.smsservice.service.MessageService;
import com.hlkj.smsservice.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MessageServiceImpl implements MessageService {

    //1、写数据的信息
    //2、与redis信息
    //3、配置参数读取的类

    @Autowired
    private MessageUser messageUser;

    @Override
    public MessageResult sendMessage(Message message) {
        MessageResult mr = null;
        String code = createCode();
        //发送短信
        Map<String, String > headers = new HashMap<>();
        //最后在header中的格式（中间是英文空格）为Authorization:APPCODE
        headers.put("Authorization", "APPCODE" + messageUser.getAppcode());
        Map<String, String> querys = new HashMap<>();
        querys.put("mobile", message.getPhone());
        querys.put("param", "code:" + code);
        querys.put("tpl_id", "TP1711063");
        Map<String, String> bodys = new HashMap<>();
        try {
            HttpResponse response = HttpUtils.doPost(messageUser.getHost(), messageUser.getPath(), messageUser.getMethod(), headers, querys, bodys);

            mr = new MessageResult();
            //1、赋值
            //2、写入数据库
        } catch (Exception e) {
            e.printStackTrace();
            mr = null;
        }
        return mr;
    }

    //产生6位编码
    private String createCode() {
        Random random = new Random();
        String message = Integer.toString(random.nextInt(899999) + 100000);
        return message;
    }
}
