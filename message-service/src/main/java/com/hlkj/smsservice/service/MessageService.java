package com.hlkj.smsservice.service;

import com.hlkj.smsservice.model.Message;
import com.hlkj.smsservice.model.MessageResult;

public interface MessageService {

    public MessageResult sendMessage(Message message);
}
