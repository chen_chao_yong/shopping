package com.hlkj.sellerservice.controller;

import com.hlkj.pojo.sys.Permission;
import com.hlkj.sellerservice.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @RequestMapping("/findAllPermissions")
    public List<Permission> findAllPermissions() {
        return permissionService.findAllPermissions();
    }
}
