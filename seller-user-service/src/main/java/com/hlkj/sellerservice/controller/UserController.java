package com.hlkj.sellerservice.controller;

import com.hlkj.pojo.sys.Permission;
import com.hlkj.pojo.sys.User;
import com.hlkj.sellerservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/findUserByUserName")
    public User findUserByUserName(String username) {
        System.out.println(username);
        return userService.findUserByUserName(username);
    }

    @RequestMapping("/findPermissionsByUserName")
    public List<Permission> findPermissionsByUserName(String username) {
        return userService.findUserPermissionsByUserName(username);
    }

    @GetMapping("/query")
    User query(@RequestParam("username") String username, @RequestParam("password") String password) {
        return userService.queryUserByNameAndPassword(username, password);
    }
}
