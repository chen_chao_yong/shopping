package com.hlkj.sellerservice.service.impl;

import com.hlkj.pojo.sys.Permission;
import com.hlkj.pojo.sys.User;
import com.hlkj.sellerservice.mapper.UserMapper;
import com.hlkj.sellerservice.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User findUserByUserName(String userName) {
        return userMapper.findUserByUsername(userName);
    }

    @Override
    public List<Permission> findUserPermissionsByUserName(String userName) {
        return userMapper.findPermissionByUsername(userName);
    }

    @Override
    public Integer addUser(User user) {
        return userMapper.addUser(user);
    }

    @Override
    public User queryUserByNameAndPassword(String name, String password) {
        User user = findUserByUserName(name);
        if(user != null){
            if (user.getPassword().equals(password)) {
                return user;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
