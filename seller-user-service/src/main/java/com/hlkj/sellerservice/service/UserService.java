package com.hlkj.sellerservice.service;

import com.hlkj.pojo.sys.Permission;
import com.hlkj.pojo.sys.User;

import java.util.List;

public interface UserService {

    public User findUserByUserName(String userName);

    public List<Permission> findUserPermissionsByUserName(String userName);

    Integer addUser(User user);

    User queryUserByNameAndPassword(String name, String password);
}
