package com.hlkj.sellerservice.service.impl;

import com.hlkj.pojo.sys.Permission;
import com.hlkj.sellerservice.mapper.PermissionMapper;
import com.hlkj.sellerservice.service.PermissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Resource
    private PermissionMapper permissionMapper;

    @Override
    public List<Permission> findAllPermissions() {
        return permissionMapper.findAllPermission();
    }
}
