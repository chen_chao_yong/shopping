package com.hlkj.sellerservice.service;

import com.hlkj.pojo.sys.Permission;

import java.util.List;

public interface PermissionService {

    public List<Permission> findAllPermissions ();
}
