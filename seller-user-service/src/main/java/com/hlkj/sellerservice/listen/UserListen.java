package com.hlkj.sellerservice.listen;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hlkj.pojo.model.Seller;
import com.hlkj.pojo.sys.User;
import com.hlkj.sellerservice.service.UserService;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserListen {

    @Autowired
    private UserService userService;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "ls.user", durable = "true"),
            exchange = @Exchange(
                    value = "ls.user.exchange",
                    ignoreDeclarationExceptions = "true",
                    type = ExchangeTypes.TOPIC),
            key = {"#.#"}))
    public void addUser(JSONObject msg) {
        Seller seller = JSON.toJavaObject(msg, Seller.class);
        User user = new User();
        user.setUsername(seller.getName());
        user.setRealname(seller.getName());
        user.setPassword(seller.getPassword());
        user.setCreateDate(new Date());
        userService.addUser(user);
    }
}
