package com.hlkj.protalweb.fegin;

import com.hlkj.pojo.model.Content;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("content-service")
public interface ContentFeignClient {

    @GetMapping("/content/findByCategoryId")
    public List<Content> findByCategoryId(@RequestParam("categoryId") Long categoryId);
}
