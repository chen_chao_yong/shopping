package com.hlkj.protalweb.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("search-service")
public interface SearchFeign {

    @RequestMapping("/goods/searchByContent")
    public String searchByContent(@RequestParam("content") String content);
}
