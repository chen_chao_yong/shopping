package com.hlkj.protalweb.fegin;

import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Content;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("redis-service")
public interface RedisFeignClient {

    @PostMapping("/list/save")
    public Result saveList(@RequestParam("key") String key, @RequestBody List<Content> contents);

    @GetMapping("/list/find")
    public List findList(@RequestParam("key") String key);
}
