package com.hlkj.protalweb.config;

import com.hlkj.protalweb.interceptor.TokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Bean
    public WebConfig getWebMvcConfig() {
        WebConfig myWebMvcConfig = new WebConfig() {
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(new TokenInterceptor()).addPathPatterns("/**")
                        .excludePathPatterns("/css/**", "/js/**", "/img/**","/data/**", "/fonts/**", "/plugins/**");
            }

            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("alogin");
                registry.addViewController("/login").setViewName("alogn");
                registry.addViewController("/main.html").setViewName("dashboard");
            }
        };
        return myWebMvcConfig;
    }
}
