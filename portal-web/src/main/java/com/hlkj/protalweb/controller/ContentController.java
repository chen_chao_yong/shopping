package com.hlkj.protalweb.controller;

import com.hlkj.pojo.file.FastDfsFile;
import com.hlkj.pojo.model.Content;
import com.hlkj.protalweb.fegin.ContentFeignClient;
import com.hlkj.protalweb.fegin.FastFileFeignClient;
import com.hlkj.protalweb.fegin.RedisFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/content")
public class ContentController {

    @Autowired
    private FastFileFeignClient fastFileFeignClient;

    @Autowired
    private ContentFeignClient contentFeignClient;

    @Autowired
    private RedisFeignClient redisFeignClient;

    @GetMapping("/findBanners")
    public List<FastDfsFile> findBanners() {
        return fastFileFeignClient.findByType(1);
    }

    @GetMapping("/findByCategoryId")
    public List<Content> findByCategoryId(@RequestParam("categoryId") Long categoryId) {
        System.out.println(categoryId);
        List banners = redisFeignClient.findList("banners");
        System.out.println(banners);
        if (banners == null||banners.size()==0) {
            //从redis中查找不到，则去数据库中查找
            banners = contentFeignClient.findByCategoryId(categoryId);
            System.out.println(banners);
            //查找出来之后，保存到redis
            redisFeignClient.saveList("banners", banners);
        }
        return banners;
    }
}
