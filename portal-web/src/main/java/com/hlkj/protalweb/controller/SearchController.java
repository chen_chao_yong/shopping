package com.hlkj.protalweb.controller;

import com.hlkj.protalweb.fegin.SearchFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private SearchFeign searchFeign;

    @RequestMapping("/searchByContent")
    @ResponseBody
    public String searchByContent(String content) {
        return searchFeign.searchByContent(content);
    }
}
