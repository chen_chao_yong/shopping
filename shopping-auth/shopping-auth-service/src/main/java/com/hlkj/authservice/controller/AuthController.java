package com.hlkj.authservice.controller;

import com.hlkj.auth.model.UserInfo;
import com.hlkj.authservice.config.JwtProperties;
import com.hlkj.authservice.service.AuthService;
import com.hlkj.common.utils.CookieUtils;
import com.hlkj.pojo.sys.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private  JwtProperties jwtProperties;
    /**
     * 登录授权
     * @return
     */
    @PostMapping("/accredit")
    public ResponseEntity<Void> authentication(@RequestBody User user, HttpServletRequest request, HttpServletResponse response) {
        // 登录校验
        System.out.println(user.getUsername());
        String token = this.authService.authentication(user.getUsername(), user.getPassword());
        if (StringUtils.isBlank(token)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        // 将token写入cookie,并指定httpOnly为true，防止通过JS获取和修改
        CookieUtils.setCookie(request, response, jwtProperties.getCookieName(),
                token, jwtProperties.getCookieMaxAge(), true);
        return ResponseEntity.ok().build();
    }

    @RequestMapping("/verify")
    public UserInfo verify(@RequestParam("token") String token) throws Exception {
        UserInfo userInfo = authService.verify(token);
        return userInfo;
    }
}
