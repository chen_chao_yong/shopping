package com.hlkj.authservice.service;

import com.hlkj.auth.model.UserInfo;
import com.hlkj.auth.utils.JwtUtils;
import com.hlkj.authservice.client.UserClient;
import com.hlkj.authservice.config.JwtProperties;
import com.hlkj.pojo.sys.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    private UserClient userClient;

    @Autowired
    private JwtProperties properties;

    public String authentication(String username, String password) {

        try {
            // 调用微服务，执行查询
            User user = this.userClient.queryUser(username, password);
            // 如果查询结果为null，则直接返回null
            if (user == null) {
                return null;
            }

            // 如果有查询结果，则生成token
            String token = JwtUtils.generateToken(new UserInfo(Long.parseLong(user.getId() + ""), user.getUsername()),
                    properties.getPrivateKey(), properties.getExpire());
            return token;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public UserInfo verify(String token) throws Exception {
        UserInfo userInfo = JwtUtils.getInfoFromToken(token, properties.getPublicKey());
        return userInfo;
    }
}
