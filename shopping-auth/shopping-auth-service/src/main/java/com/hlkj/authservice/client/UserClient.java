package com.hlkj.authservice.client;

import com.hlkj.pojo.sys.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("seller-user-service")
public interface UserClient {

    @GetMapping("/user/query")
    User queryUser(@RequestParam("username") String username, @RequestParam("password") String password);
}
