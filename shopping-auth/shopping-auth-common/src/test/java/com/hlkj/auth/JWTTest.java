package com.hlkj.auth;

import com.hlkj.auth.model.UserInfo;
import com.hlkj.auth.utils.JwtUtils;
import com.hlkj.auth.utils.RsaUtils;
import org.junit.Before;
import org.junit.Test;

import java.security.PrivateKey;
import java.security.PublicKey;

;

public class JWTTest {

    private static final String pubKeyPath = "C:\\temp\\rsa\\rsa.pub";

    private static final String priKeyPath = "C:\\temp\\rsa\\rsa.pri";

    private PublicKey publicKey;

    private PrivateKey privateKey;

    @Test
    public void testRsa()throws Exception {
        RsaUtils.generateKey(pubKeyPath, priKeyPath, "234");
    }

    @Before
    public void testGetRsa() throws Exception {
        this.publicKey = RsaUtils.getPublicKey(pubKeyPath);
        this.privateKey = RsaUtils.getPrivateKey(priKeyPath);
    }

    @Test
    public void testGenerateToken() throws Exception {
        // 生成token
        String token = JwtUtils.generateToken(new UserInfo(20L, "jack"), privateKey, 5);
        System.out.println("token = " + token);
    }

    @Test
    public void testParseToken() throws Exception {
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpZCI6MjAsInVzZXJuYW1lIjoiamFjayIsImV4cCI6MTU5NjMzODU5OX0.FNQ7hHDKqtALoa80mgIozBZ0raxqZ5q3qcfzGrVlmmIAPnsgUsQ52lKkgookACwT80Dif8r25_mzZ8x4F9Lriw5hJor20fEUazQktfECivIMKkRUd3Iyfztn1KmAi17RE3E64By8CVUnZiwvsVScKv_fmLBnELIlXflET-RhGtU";

        // 解析token
        UserInfo user = JwtUtils.getInfoFromToken(token, publicKey);
        System.out.println("id: " + user.getId());
        System.out.println("userName: " + user.getUsername());
    }
}
