package com.hlkj.auth.utils;

import org.apache.commons.lang3.StringUtils;

public class ObjectUtils {
    //对象转换成字符串
    public static String toString(Object obj) {
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }
    //对象转成Long
    public static Long toLong(Object obj) {
        if (obj == null) {
            return 0L;
        }
        if (obj instanceof Double || obj instanceof Float) {
            return Long.valueOf(StringUtils.substringBefore(obj.toString(), "."));
        }
        if (obj instanceof Number) {
            return Long.valueOf(obj.toString());
        }
        if (obj instanceof String) {
            return Long.valueOf(obj.toString());
        } else {
            return 0L;
        }
    }
    //对象转成数字
    public static Integer toInt(Object obj) {
        return toLong(obj).intValue();
    }
}