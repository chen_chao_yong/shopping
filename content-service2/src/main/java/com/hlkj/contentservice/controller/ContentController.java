package com.hlkj.contentservice.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Content;
import com.hlkj.contentservice.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/content")
public class ContentController {

    @Autowired
    private ContentService contentService;

    @GetMapping("/findAll")
    public List<Content> findAll(){
        return contentService.findAll();
    }
    @RequestMapping("/findPage")
    public PageResult findPage(Integer pageNo,Integer pageSize){
        return contentService.findPage(pageNo,pageSize);
    }

    @PostMapping("/add")
    public Result add(@RequestBody Content content){
        return contentService.add(content);
    }

    @PostMapping("/update")
    public Result update(@RequestBody Content content){
        return contentService.update(content);
    }

    @RequestMapping("/findOne")
    public Content findOne(Long id){
        return contentService.findOne(id);
    }

    @PostMapping("/search")
    public PageResult search(@RequestParam("pageNo") Integer pageNo, @RequestParam("pageSize") Integer pageSize,
                             @RequestBody Content content){
        System.out.println(content.getTitle());
        return  contentService.search(pageNo,pageSize,content);
    }

    @RequestMapping("/delete")
    public Result delete(@RequestParam("ids") Long[] ids){
        return contentService.delete(ids);
    }

    @RequestMapping("/findByCategoryId")
    public List<Content> findByCategoryId(Long categoryId){
        return  contentService.findByCategoryId(categoryId);
    }
}
