package com.hlkj.contentservice.service;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Content;

import java.util.List;

public interface ContentService {

    List<Content> findAll();
    PageResult findPage(Integer pageSize, Integer pageNo);
    Result add(Content brand);
    Content findOne(Long id);
    Result update(Content brand);
    PageResult search(Integer pageNo, Integer pageSize, Content brand);
    Result delete(Long[] ids);

    public List<Content> findByCategoryId(Long categoryId);
}
