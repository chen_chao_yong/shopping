package com.hlkj.esservice.service;

import com.hlkj.esservice.model.Goods;
import org.springframework.data.domain.Page;

public interface GoodsService {

    public Page<Goods> findByName(String name);
}
