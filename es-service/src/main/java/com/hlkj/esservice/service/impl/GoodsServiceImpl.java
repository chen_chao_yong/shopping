package com.hlkj.esservice.service.impl;

import com.hlkj.esservice.dao.ElasticRepository;
import com.hlkj.esservice.model.Goods;
import com.hlkj.esservice.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Service;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Autowired
    private ElasticRepository elasticRepository;

    private Pageable pageable = PageRequest.of(0, 10);

    @Override
    public Page<Goods> findByName(String name) {
        return elasticRepository.findByContent(name, pageable);
    }
}
