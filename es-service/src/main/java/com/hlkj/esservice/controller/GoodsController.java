package com.hlkj.esservice.controller;

import com.hlkj.esservice.model.Goods;
import com.hlkj.esservice.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @GetMapping("/searchByContent")
    public Page<Goods> findByName(String content) {
        return goodsService.findByName(content);
    }
}
