package com.hlkj.esservice.dao;

import com.hlkj.esservice.model.Goods;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ElasticRepository extends ElasticsearchRepository<Goods, Long> {
    //默认注释
    @Query
    Page<Goods> findByContent(String content, Pageable pageable);

    @Query
    Page<Goods> findByFirstCode(String firstCode, Pageable pageable);

    @Query
    Page<Goods> findBySecondCode(String SecondCode, Pageable pageable);
}
