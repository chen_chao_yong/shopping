package com.hlkj.esservice;

import com.hlkj.esservice.model.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

@SpringBootTest
class EsServiceApplicationTests {

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Test
    void contextLoads() {
        //创建索引
        this.elasticsearchRestTemplate.createIndex(Goods.class);
        //配置映射
        this.elasticsearchRestTemplate.putMapping(Goods.class);
    }

}
