package com.hlkj.fastdfsservice.controller;

import com.hlkj.fastdfsservice.service.FastDfsService;
import com.hlkj.pojo.file.FastDfsFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/fastdfs")
public class FastDfsFileController {

    @Autowired
    private FastDfsService fastDfsService;

    @PostMapping("/upload")
    public FastDfsFile upload(@RequestPart(value = "file", required = false) MultipartFile file) {
        return fastDfsService.upload(file);
    }

    @GetMapping("/download")
    public byte[] download(Long id) {
        return fastDfsService.download(id);
    }

    @GetMapping("/findByType")
    public List<FastDfsFile> findByType(Integer type) {
        return fastDfsService.findByType(type);
    }
}
