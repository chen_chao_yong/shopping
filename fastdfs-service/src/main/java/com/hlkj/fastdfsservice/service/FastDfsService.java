package com.hlkj.fastdfsservice.service;

import com.hlkj.pojo.file.FastDfsFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FastDfsService {

    public FastDfsFile upload(MultipartFile file);

    public byte[] download(Long id);

    public List<FastDfsFile> findByType(Integer type);
}
