package com.hlkj.fastdfsservice.service.impl;

import com.hlkj.fastdfsservice.Mapper.FastDfsFileMapper;
import com.hlkj.fastdfsservice.service.FastDfsService;
import com.hlkj.pojo.file.FastDfsFile;
import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class FastDfsServiceImpl implements FastDfsService {

    //fastDfs客户端工具
    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Resource
    private FastDfsFileMapper fastDfsFileMapper;

    @Override
    public FastDfsFile upload(MultipartFile file) {
        //1、上传到那一个group1    //2、上传文件的扩展名   //3、上传后返回的是什么
        FastDfsFile fastDfsFile = null;
        try{
            StorePath storePath=null;
            //文件名：上传的时候的文件名
            String fileName = file.getOriginalFilename();
            //上传时的kuozhanming
            String extendName = FilenameUtils.getExtension(fileName);
            //保存到fastdfs
            storePath = fastFileStorageClient.uploadFile("group1", file.getBytes(), extendName);
            //保存到数据库上去
            fastDfsFile = new FastDfsFile(fileName,"http://47.107.135.110:8090","group1",storePath.getFullPath(), new Date());

            fastDfsFileMapper.add(fastDfsFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fastDfsFile;
    }

    @Override
    public byte[] download(Long id) {
        FastDfsFile fastDfsFile = fastDfsFileMapper.findOne(id);
        //从fastdfs上下载数据流
        byte[] bytes = fastFileStorageClient.downloadFile(fastDfsFile.getGroupname(), fastDfsFile.getPath().substring(fastDfsFile.getPath().indexOf("/")+1));
        return bytes;
    }

    @Override
    public List<FastDfsFile> findByType(Integer type) {
        return fastDfsFileMapper.findByType(type);
    }


}
