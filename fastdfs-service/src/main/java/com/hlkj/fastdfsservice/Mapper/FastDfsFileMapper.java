package com.hlkj.fastdfsservice.Mapper;

import com.hlkj.pojo.file.FastDfsFile;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface FastDfsFileMapper {

    @Insert("insert into fastdfs_file(server, name,  groupname, path, create_time)" +
            "values(#{server}, #{name}, #{groupname} , #{path}, #{createTime})")
    public int add(FastDfsFile file);

    @Select("select id, server, name, groupname, path, create_time as createTime "+
            "from fastdfs_file where id=#{id}")
    public FastDfsFile findOne(Long id);

    @Select("select id, server, name, groupname, path, create_time as createTime "+
            "from fastdfs_file where type=#{type}")
    public List<FastDfsFile> findByType(Integer type);
}
