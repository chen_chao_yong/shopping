package com.hlkj.rabbitmqdemo.direct;

import com.hlkj.rabbitmqdemo.utils.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.util.Random;

public class Provider {
    public static void main(String[] args) throws Exception {
        //创建连接
        Connection connection = ConnectionUtil.getConnection();
        //获得通道
        Channel channel = connection.createChannel();
        //声明exchange,指定类型为fanout
        channel.exchangeDeclare("direct_exchange", "direct");
        String msg = "";
        String[] routekeys = {"insert", "update", "delete"};
        Random random = new Random();
        //发布信息
        for (int i = 0; i < 10; i++) {
            msg = "msg" + (i + 1);
            channel.basicPublish("direct_exchange", routekeys[random.nextInt(2)], null, msg.getBytes());
            System.out.println("send msg" + (i + 1));
        }
        //关闭通道
        channel.close();
        //关闭连接
        connection.close();
    }
}
