package com.hlkj.rabbitmqdemo.direct;

import com.hlkj.rabbitmqdemo.utils.ConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;

public class Reciever2 {

    public static void main(String[] args) throws Exception {
        //创建连接
        Connection connection = ConnectionUtil.getConnection();
        //获得通道
        Channel channel = connection.createChannel();
        //声明exchange, 指定类型为fanout
        channel.exchangeDeclare("direct_exchange", "direct");
        //队列绑定
        channel.queueBind("direct_queue2", "direct_exchange", "delete");
        channel.queueBind("direct_queue2", "direct_exchange", "update");
        channel.queueBind("direct_queue2", "direct_exchange", "insert");
        //设置一次只能去取一条信息
//        channel.basicQos(1);
        //创建消费者
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {

                String msg = new String(body);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("接收到: " + msg);
                //channel.basicAck(envelope.getDeliveryTag(), false);
            }
        };
        channel.basicConsume("direct_queue2", true, defaultConsumer);
    }
}
