package com.hlkj.rabbitmqdemo.basic.producter;

import com.hlkj.rabbitmqdemo.utils.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class Send {

    public static void main(String[] args) {
        try {
            //获取连接
            Connection connection = ConnectionUtil.getConnection();
            //从连接中创建通道
            Channel channel = connection.createChannel();
            //声明队列
            channel.queueDeclare("user", false, false, false, null);
            //信息内容
            String msg = "Hello world";
            channel.basicPublish("", "user", null, msg.getBytes());
            System.out.println("send message is success!");
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
