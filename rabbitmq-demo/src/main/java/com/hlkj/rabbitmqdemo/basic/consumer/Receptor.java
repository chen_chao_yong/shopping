package com.hlkj.rabbitmqdemo.basic.consumer;

import com.hlkj.rabbitmqdemo.utils.ConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;

public class Receptor {

    public static void main(String[] args) throws Exception {
        //获取连接
        Connection connection = ConnectionUtil.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //声明队列
        channel.queueDeclare("user", false, false, false, null);
        //定义队列消费者
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            //获取消息
            public void handleDelivery(String consumerTag , Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body);
                System.out.println("received: " + msg + "!");
                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        };
        channel.basicConsume("user", false, consumer);
    }
}
