package com.hlkj.rabbitmqdemo.utils;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class ConnectionUtil {

    public static Connection getConnection() throws Exception {
        //定义工厂
        ConnectionFactory factory = new ConnectionFactory();
        //设置 服务接口
        factory.setHost("47.107.135.110");
        //设置端口
        factory.setPort(5672);
        //设置连接虚拟机
        factory.setVirtualHost("ls2017");
        //设置连接用户名与密码
        factory.setUsername("ls_user");
        factory.setPassword("12345");
        //获取连接
        Connection connection = factory.newConnection();
        return connection;
    }
}
