package com.hlkj.rabbitmqdemo.workqueue;

import com.hlkj.rabbitmqdemo.utils.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class Provider {

    public static void main(String[] args) throws Exception {
        //创建连接
        Connection connection = ConnectionUtil.getConnection();
        //获得通道
        Channel channel = connection.createChannel();
        //声明队列
        channel.queueDeclare("user", false, false, false, null);
        String msg = "";
        //发布信息
        for (int i = 0; i < 50; i++) {
            msg = "msg" + (i + 1);
            channel.basicPublish("", "user", null, msg.getBytes());
            System.out.println("send msg" + (i + 1));
        }
        //关闭通道
        channel.close();
        //关闭连接
        connection.close();
    }
}
