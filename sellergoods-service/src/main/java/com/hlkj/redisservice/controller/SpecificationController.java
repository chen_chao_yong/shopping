package com.hlkj.redisservice.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Specification;
import com.hlkj.redisservice.service.SpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/specification")
public class SpecificationController {

    @Autowired
    private SpecificationService specificationService;

    @GetMapping("/findAll")
    public List<Specification> findAll() {
        return specificationService.findAll();
    }

    @RequestMapping("/findPage")
    public PageResult findPage(Integer pageNo, Integer pageSize){
        return specificationService.findPage(pageNo, pageSize);
    }

    @PostMapping("/add")
    public Result add(@RequestBody Specification specification) {
        return specificationService.add(specification);
    }

    @PostMapping("/update")
    public Result update(@RequestBody Specification specification) {
        return specificationService.update(specification);
    }

    @GetMapping("/findOne")
    public Specification findOne(Long id) {
        return specificationService.findOne(id);
    }

    @PostMapping("/search")
    public PageResult update(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             @RequestBody Specification specification) {
        return specificationService.search(pageNo, pageSize, specification);
    }

    @RequestMapping("/delete")
    public  Result delete(@RequestParam("ids") Long[] ids) {
        return specificationService.delete(ids);
    }

    @RequestMapping("/selectOptionList")
    public List<Map> seleteOptionList(){
        return specificationService.seleteOptionList();
    }
}
