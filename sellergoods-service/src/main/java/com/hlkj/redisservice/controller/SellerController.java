package com.hlkj.redisservice.controller;

import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Seller;
import com.hlkj.redisservice.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/seller")
public class SellerController {

    @Autowired
    private SellerService sellerService;

    @PostMapping("/add")
    public Result add(@RequestBody Seller seller) {
        return sellerService.add(seller);
    }
}
