package com.hlkj.redisservice.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.pojo.model.Goods;
import com.hlkj.redisservice.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @PostMapping("/search")
    public PageResult search(@RequestParam("page") Integer page, @RequestParam("rows") Integer rows,
                             @RequestBody Goods goods) {
        return goodsService.search(page,rows,goods);
    }

    @GetMapping("/findOne")
    public Goods findOne(@RequestParam("id") Long id) {
        return goodsService.findOne(id);
    }
}
