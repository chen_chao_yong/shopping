package com.hlkj.redisservice.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.TypeTemplate;
import com.hlkj.redisservice.service.TypeTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/typeTemplate")
public class TypeTemplateController {

    @Autowired
    private TypeTemplateService typeTemplateService;

    @GetMapping("/findAll")
    public List<TypeTemplate> findAll() {
        return typeTemplateService.findAll();
    }

    @RequestMapping("/findPage")
    public PageResult findPage(Integer pageNo, Integer pageSize){
        return typeTemplateService.findPage(pageNo, pageSize);
    }

    @PostMapping("/add")
    public Result add(@RequestBody TypeTemplate typeTemplate) {
        return typeTemplateService.add(typeTemplate);
    }

    @PostMapping("/update")
    public Result update(@RequestBody TypeTemplate typeTemplate) {
        return typeTemplateService.update(typeTemplate);
    }

    @GetMapping("/findOne")
    public TypeTemplate findOne(Long id) {
        return typeTemplateService.findOne(id);
    }

    @PostMapping("/search")
    public PageResult update(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             @RequestBody TypeTemplate typeTemplate) {
        System.out.println(pageNo+"-----"+pageSize+"------"+typeTemplate.getName());
        return typeTemplateService.search(pageNo, pageSize, typeTemplate);
    }

    @RequestMapping("/delete")
    public  Result delete(@RequestParam("ids") Long[] ids) {
        return typeTemplateService.delete(ids);
    }
}
