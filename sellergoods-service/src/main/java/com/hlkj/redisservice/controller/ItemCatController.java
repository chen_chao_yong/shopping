package com.hlkj.redisservice.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.ItemCat;
import com.hlkj.redisservice.service.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/itemCat")
public class ItemCatController {

    @Autowired
    private ItemCatService itemCatService;

    @GetMapping("/findAll")
    public List<ItemCat> findAll() {
        return itemCatService.findAll();
    }

    @RequestMapping("/findPage")
    public PageResult findPage(Integer pageNo, Integer pageSize){
        return itemCatService.findPage(pageNo, pageSize);
    }

    @PostMapping("/add")
    public Result add(@RequestBody ItemCat itemCat) {
        return itemCatService.add(itemCat);
    }

    @PostMapping("/update")
    public Result update(@RequestBody ItemCat itemCat) {
        return itemCatService.update(itemCat);
    }

    @GetMapping("/findOne")
    public ItemCat findOne(Long id) {
        return itemCatService.findOne(id);
    }

    @PostMapping("/search")
    public PageResult update(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             @RequestBody ItemCat itemCat) {
        return itemCatService.search(pageNo, pageSize, itemCat);
    }

    @RequestMapping("/delete")
    public  Result delete(@RequestParam("ids") Long[] ids) {
        return itemCatService.delete(ids);
    }

    @GetMapping("/searchItemCatByParentId")
    public List<ItemCat> searchItemCatByParentId(Long parentId) {
        System.out.println(parentId);
        return itemCatService.searchItemCatByParentId(parentId);
    }
}
