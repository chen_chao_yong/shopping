package com.hlkj.redisservice.controller;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Brand;
import com.hlkj.redisservice.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    @GetMapping("/findAll")
    public List<Brand> findAll() {
        return brandService.findAll();
    }

    @RequestMapping("/findPage")
    public PageResult findPage(Integer pageNo, Integer pageSize){
        return brandService.findPage(pageNo, pageSize);
    }

    @PostMapping("/add")
    public Result add(@RequestBody Brand brand) {
        return brandService.add(brand);
    }

    @PostMapping("/update")
    public Result update(@RequestBody Brand brand) {
        return brandService.update(brand);
    }

    @GetMapping("/findOne")
    public Brand findOne(Long id) {
        return brandService.findOne(id);
    }

    @PostMapping("/search")
    public PageResult update(@RequestParam("pageNo") Integer pageNo,
                             @RequestParam("pageSize") Integer pageSize,
                             @RequestBody Brand brand) {
        return brandService.search(pageNo, pageSize, brand);
    }

    @RequestMapping("/delete")
    public  Result delete(@RequestParam("ids") Long[] ids) {
        return brandService.delete(ids);
    }

    @RequestMapping("/selectOptionList")
    public  List<Map> selectOptionList() {
        return brandService.seleteOptionList();
    }
}
