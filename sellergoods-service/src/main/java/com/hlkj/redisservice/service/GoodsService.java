package com.hlkj.redisservice.service;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Goods;

public interface GoodsService {

    Result add(Goods record) throws Exception;

    PageResult search(Integer pageNo, Integer pageSize, Goods record);

    Goods findOne(Long id);
}
