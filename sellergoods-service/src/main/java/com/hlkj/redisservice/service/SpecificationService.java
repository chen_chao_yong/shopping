package com.hlkj.redisservice.service;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Specification;

import java.util.List;
import java.util.Map;

public interface SpecificationService {

    List<Specification> findAll();

    PageResult findPage(Integer pageNo, Integer pageSize);

    Result add(Specification record);

    Result update(Specification record);

    Specification findOne(Long id);

    PageResult search(Integer pageNo, Integer pageSize, Specification record);

    Result delete(Long[] id);

    List<Map> seleteOptionList();
}
