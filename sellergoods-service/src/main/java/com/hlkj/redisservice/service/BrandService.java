package com.hlkj.redisservice.service;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Brand;

import java.util.List;
import java.util.Map;

public interface BrandService {

    List<Brand> findAll();

    PageResult findPage(Integer pageNo, Integer pageSize);

    Result add(Brand brand);

    Result update(Brand brand);

    Brand findOne(Long id);

    PageResult search(Integer pageNo, Integer pageSize, Brand brand);

    Result delete(Long[] id);

    List<Map> seleteOptionList();

    Result batch(List<Brand> brandList);
}
