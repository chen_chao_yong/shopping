package com.hlkj.redisservice.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Brand;
import com.hlkj.pojo.model.BrandExample;
import com.hlkj.pojo.model.BrandExample.Criteria;
import com.hlkj.redisservice.mapper.BrandMapper;
import com.hlkj.redisservice.service.BrandService;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class BrandServiceImpl implements BrandService
{

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public List<Brand> findAll() {
        return brandMapper.selectByExample(null);
    }

    @Override
    public PageResult findPage(Integer pageNo, Integer pageSize) {
        //分页查询空间
        PageHelper.startPage(pageNo, pageSize);
        //分页查询
        Page<Brand> page = (Page<Brand>)brandMapper.selectByExample(null);
        //返回结果转换
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public Result add(Brand brand) {
        Integer ret = brandMapper.insert(brand);
        if (ret > 0) {
            return new Result(true, "添加成功");
        } else {
            return new Result(false, "添加失败");
        }
    }

    @Override
    public Result update(Brand brand) {
        Integer ret=brandMapper.updateByPrimaryKey(brand);
        if (ret > 0) {
            return new Result(true, "修改成功");
        } else {
            return new Result(false, "修改失败");
        }
    }

    @Override
    public Brand findOne(Long id) {
        return brandMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult search(Integer pageNo, Integer pageSize, Brand brand) {
        if (pageNo <= 0){
            pageNo = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }
        if (brand == null||brand.getName() == null||brand.getName() == "") {
            return  findPage(pageNo, pageSize);
        }
        //构造查询的example
        BrandExample example = new BrandExample();
        //构建查询条件
        Criteria criteria = example.createCriteria();
        criteria.andNameLike("%" + brand.getName() + "%");

        PageHelper.startPage(pageNo, pageSize);

        Page<Brand> page = (Page<Brand>)brandMapper.selectByExample(example);

        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private SqlSessionFactory sqlSessionFactory;
    @Override
    public Result delete(Long[] ids) {
        if (ids.length > 0) {
            for (int i = 0; i < ids.length; i++) {
                brandMapper.deleteByPrimaryKey(ids[i]);
            }

//            SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
//            try {
//                Brand brand = sqlSession.getMapper(Brand.class);
//                for (Long id : ids) {
//                    brandMapper.deleteByPrimaryKey(id);
//                }
//                sqlSession.commit();
//            } catch (Exception e) {
//                e.printStackTrace();
//                sqlSession.rollback();
//            } finally {
//                sqlSession.close();
//            }
            return new Result(true, "删除成功!");
        } else {
            return new Result(false, "没有要删除的id");
        }
    }

    @Override
    public List<Map> seleteOptionList() {
        return brandMapper.seleteOptionList();
    }

    @Override
    public Result batch(List<Brand> brandList) {
        Result result = null;
        try{
            //1.插入优化：   原因：数据库连接是需要时间的  2.数据库在操作一个代码时，所需的时间要大大的减：代码转义也是要时间
            //2.数据量的大小：一次性存多少条
            int index = 0;
            List<Brand> tempBrands = new ArrayList<>();
            for (int i = 0; i < brandList.size(); i++) {
                //小于数量时，添加到临时
                if (index < 100) {
                    tempBrands.add(brandList.get(i));
                    index++;
                }
                //等于时，则进行保存
                if (index == 100) {
                    brandMapper.batch(tempBrands);
                    tempBrands = new ArrayList<>();
                    index = 0;
                }
            }
            if (index > 0) {
                brandMapper.batch(tempBrands);
            }
            result = new Result(true, "插入成功：" +brandList.size()+"条");
        }catch (Exception e) {
            e.printStackTrace();
            result = new Result(false, "插入失败");
        }
        return null;
    }
}
