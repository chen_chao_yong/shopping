package com.hlkj.redisservice.service;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.TypeTemplate;

import java.util.List;

public interface TypeTemplateService {

    List<TypeTemplate> findAll();

    PageResult findPage(Integer pageNo, Integer pageSize);

    Result add(TypeTemplate record);

    Result update(TypeTemplate record);

    TypeTemplate findOne(Long id);

    PageResult search(Integer pageNo, Integer pageSize, TypeTemplate record);

    Result delete(Long[] id);
}
