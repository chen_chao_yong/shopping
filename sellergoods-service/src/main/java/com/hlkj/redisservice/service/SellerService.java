package com.hlkj.redisservice.service;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Seller;

import java.util.List;

public interface SellerService {

    List<Seller> findAll();

    PageResult findPage(Integer pageNo, Integer pageSize);

    Result add(Seller seller);

    Result update(Seller seller);

    Seller findOne(String id);

    PageResult search(Integer pageNo, Integer pageSize, Seller seller);

    Result delete(Long[] id);
}
