package com.hlkj.redisservice.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.TypeTemplate;
import com.hlkj.pojo.model.TypeTemplateExample;
import com.hlkj.pojo.model.TypeTemplateExample.Criteria;
import com.hlkj.redisservice.mapper.TypeTemplateMapper;
import com.hlkj.redisservice.service.TypeTemplateService;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeTemplateServiceImpl implements TypeTemplateService
{

    @Autowired
    private TypeTemplateMapper typeTemplateMapper;

    @Override
    public List<TypeTemplate> findAll() {
        return typeTemplateMapper.selectByExample(null);
    }

    @Override
    public PageResult findPage(Integer pageNo, Integer pageSize) {
        //分页查询空间
        PageHelper.startPage(pageNo, pageSize);
        //分页查询
        Page<TypeTemplate> page = (Page<TypeTemplate>)typeTemplateMapper.selectByExample(null);
        //返回结果转换
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public Result add(TypeTemplate typeTemplate) {
        Integer ret = typeTemplateMapper.insert(typeTemplate);
        if (ret > 0) {
            return new Result(true, "添加成功");
        } else {
            return new Result(false, "添加失败");
        }
    }

    @Override
    public Result update(TypeTemplate typeTemplate) {
        Integer ret=typeTemplateMapper.updateByPrimaryKey(typeTemplate);
        if (ret > 0) {
            return new Result(true, "修改成功");
        } else {
            return new Result(false, "修改失败");
        }
    }

    @Override
    public TypeTemplate findOne(Long id) {
        return typeTemplateMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult search(Integer pageNo, Integer pageSize, TypeTemplate typeTemplate) {
        if (pageNo <= 0){
            pageNo = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }
        if (typeTemplate == null||typeTemplate.getName() == null||typeTemplate.getName() == "") {
            return  findPage(pageNo, pageSize);
        }
        //构造查询的example
        TypeTemplateExample example = new TypeTemplateExample();
        //构建查询条件
        Criteria criteria = example.createCriteria();
        criteria.andNameLike("%" + typeTemplate.getName() + "%");

        PageHelper.startPage(pageNo, pageSize);

        Page<TypeTemplate> page = (Page<TypeTemplate>)typeTemplateMapper.selectByExample(example);

        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private SqlSessionFactory sqlSessionFactory;
    @Override
    public Result delete(Long[] ids) {
        if (ids.length > 0) {
            for (int i = 0; i < ids.length; i++) {
                typeTemplateMapper.deleteByPrimaryKey(ids[i]);
            }

//            SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
//            try {
//                TypeTemplate typeTemplate = sqlSession.getMapper(TypeTemplate.class);
//                for (Long id : ids) {
//                    typeTemplateMapper.deleteByPrimaryKey(id);
//                }
//                sqlSession.commit();
//            } catch (Exception e) {
//                e.printStackTrace();
//                sqlSession.rollback();
//            } finally {
//                sqlSession.close();
//            }
            return new Result(true, "删除成功!");
        } else {
            return new Result(false, "没有要删除的id");
        }
    }
}
