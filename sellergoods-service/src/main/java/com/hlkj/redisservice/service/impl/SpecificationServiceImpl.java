package com.hlkj.redisservice.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Specification;
import com.hlkj.pojo.model.SpecificationExample;
import com.hlkj.pojo.model.SpecificationExample.Criteria;
import com.hlkj.pojo.model.SpecificationOption;
import com.hlkj.pojo.model.SpecificationOptionExample;
import com.hlkj.redisservice.mapper.SpecificationMapper;
import com.hlkj.redisservice.mapper.SpecificationOptionMapper;
import com.hlkj.redisservice.service.SpecificationService;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class  SpecificationServiceImpl implements SpecificationService

{
    //主表上的Mapper
    @Autowired
    private SpecificationMapper specificationMapper;

    //子表的Mapper
    @Autowired
    private SpecificationOptionMapper specificationOptionMapper;

    @Override
    public List<Specification> findAll() {
        return specificationMapper.selectByExample(null);
    }

    @Override
    public PageResult findPage(Integer pageNo, Integer pageSize) {
        //分页查询空间
        PageHelper.startPage(pageNo, pageSize);
        //分页查询
        Page<Specification> page = (Page<Specification>)specificationMapper.selectByExample(null);
        //返回结果转换
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public Result add(Specification specification) {
        try {
            Long ret = specificationMapper.insert(specification);
            System.out.println(specification.getId()+"----"+ret);
            //对象的持久态：
            List<SpecificationOption> options = specification.getSpecificationOptionList();
            for (int i = 0; i < options.size(); i++) {
                SpecificationOption option = options.get(i);
                option.setSpecId(specification.getId());
                specificationOptionMapper.insert(options.get(i));
            }
            return new Result(true, "添加成功");
        }catch (Exception e) {
            return new Result(false, "添加失败");
        }
    }

    @Override
    public Result update(Specification specification) {
        try {
            //option中有添加、删除、修改
            //1、修改主表
            Integer ret = specificationMapper.updateByPrimaryKey(specification);
            //2、删除子表的所有这个id的option
            specificationOptionMapper.deleteSourceOption(specification.getId());
            //3、重新保存
            List<SpecificationOption> options = specification.getSpecificationOptionList();
            for (int i = 0; i < options.size(); i++) {
                SpecificationOption option = options.get(i);
                option.setSpecId(specification.getId());
                specificationOptionMapper.insert(options.get(i));
            }
            return new Result(true, "修改成功");
        } catch (Exception e) {
            return new Result(false, "修改失败");
        }
    }

    @Override
    public Specification findOne(Long id) {
        //双表操作的方法：多表查询，多次查询
        //第一步：找出主表的数据
        Specification specification = specificationMapper.selectByPrimaryKey(id);
        ///第二步：找出子数据
        List<SpecificationOption> specificationOptionList = specificationOptionMapper.searchSpecificationOptions(id);
        //设置参数
        specification.setSpecificationOptionList(specificationOptionList);
        return specification;
    }

    @Override
    public PageResult search(Integer pageNo, Integer pageSize, Specification specification) {
        if (pageNo <= 0){
            pageNo = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }
        if (specification == null||specification.getSpecName() == null||specification.getSpecName() == "") {
            return  findPage(pageNo, pageSize);
        }
        //构造查询的example
        SpecificationExample example = new SpecificationExample();
        //构建查询条件
        Criteria criteria = example.createCriteria();
        criteria.andSpecNameLike("%" + specification.getSpecName() + "%");

        PageHelper.startPage(pageNo, pageSize);

        Page<Specification> page = (Page<Specification>)specificationMapper.selectByExample(example);

        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private SqlSessionFactory sqlSessionFactory;
    @Override
    public Result delete(Long[] ids) {
        try {
            for (int i = 0; i < ids.length; i++) {
                specificationMapper.deleteByPrimaryKey(ids[i]);
                SpecificationOptionExample example = new SpecificationOptionExample();
                example.createCriteria().andSpecIdEqualTo(ids[i]);
                specificationOptionMapper.deleteByExample(example);
            }
            return new Result(true, "删除成功!");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "没有要删除的id");
        }
    }

    @Override
    public List<Map> seleteOptionList() {
        return specificationMapper.selectOptionList();
    }
}
