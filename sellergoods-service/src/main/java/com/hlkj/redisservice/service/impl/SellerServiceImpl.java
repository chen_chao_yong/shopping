package com.hlkj.redisservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.Seller;
import com.hlkj.pojo.model.SellerExample;
import com.hlkj.pojo.model.SellerExample.Criteria;
import com.hlkj.redisservice.mapper.SellerMapper;
import com.hlkj.redisservice.service.SellerService;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SellerServiceImpl implements SellerService
{

    @Autowired
    private SellerMapper sellerMapper;

    @Override
    public List<Seller> findAll() {
        return sellerMapper.selectByExample(null);
    }

    @Override
    public PageResult findPage(Integer pageNo, Integer pageSize) {
        //分页查询空间
        PageHelper.startPage(pageNo, pageSize);
        //分页查询
        Page<Seller> page = (Page<Seller>)sellerMapper.selectByExample(null);
        //返回结果转换
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Override
    public Result add(Seller seller) {
        Integer ret = sellerMapper.insert(seller);
        if (ret > 0) {
            amqpTemplate.convertAndSend("user." + "add", JSON.toJSON(seller));
            return new Result(true, "添加成功");
        } else {
            return new Result(false, "添加失败");
        }
    }

    @Override
    public Result update(Seller seller) {
        Integer ret=sellerMapper.updateByPrimaryKey(seller);
        if (ret > 0) {
            return new Result(true, "修改成功");
        } else {
            return new Result(false, "修改失败");
        }
    }

    @Override
    public Seller findOne(String id) {
        return sellerMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult search(Integer pageNo, Integer pageSize, Seller seller) {
        if (pageNo <= 0){
            pageNo = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }
        if (seller == null||seller.getName() == null||seller.getName() == "") {
            return  findPage(pageNo, pageSize);
        }
        //构造查询的example
        SellerExample example = new SellerExample();
        //构建查询条件
        Criteria criteria = example.createCriteria();
        criteria.andNameLike("%" + seller.getName() + "%");

        PageHelper.startPage(pageNo, pageSize);

        Page<Seller> page = (Page<Seller>)sellerMapper.selectByExample(example);

        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private SqlSessionFactory sqlSessionFactory;
    @Override
    public Result delete(Long[] ids) {
        if (ids.length > 0) {
            for (int i = 0; i < ids.length; i++) {
//                sellerMapper.deleteByPrimaryKey(ids[i]);
            }

//            SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
//            try {
//                Seller seller = sqlSession.getMapper(Seller.class);
//                for (Long id : ids) {
//                    sellerMapper.deleteByPrimaryKey(id);
//                }
//                sqlSession.commit();
//            } catch (Exception e) {
//                e.printStackTrace();
//                sqlSession.rollback();
//            } finally {
//                sqlSession.close();
//            }
            return new Result(true, "删除成功!");
        } else {
            return new Result(false, "没有要删除的id");
        }
    }
}
