package com.hlkj.redisservice.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.*;
import com.hlkj.pojo.model.TbGoodsExample.Criteria;
import com.hlkj.redisservice.mapper.GoodsDescMapper;
import com.hlkj.redisservice.mapper.ItemMapper;
import com.hlkj.redisservice.mapper.TbGoodsMapper;
import com.hlkj.redisservice.service.GoodsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService
{

    @Resource
    private TbGoodsMapper tbGoodsMapper;
    
    @Resource
    private GoodsDescMapper goodsDescMapper;
    
    @Resource
    private ItemMapper itemMapper;

    @Transactional
    @Override
    public Result add(Goods goods){

        //同时对三个表进行操作，要么都成功，要么都不成功
        tbGoodsMapper.insert(goods.getGoods());
        goodsDescMapper.insert(goods.getGoodsDesc());
        List<Item> itemList = goods.getItems();
        for (int i = 0; i < itemList.size(); i++) {
            itemMapper.insert(itemList.get(i));
        }
        return null;
    }

    @Override
    public PageResult search(Integer pageNo, Integer pageSize, Goods goods) {

        PageHelper.startPage(pageNo, pageSize);
        TbGoodsExample example = null;
        Criteria criteria = null;

        if (goods == null) {
            example = new TbGoodsExample();
            criteria = example.createCriteria();
        }
        //查找到TbGoods的分页信息
        Page<TbGoods> pagetbGoods = (Page<TbGoods>) tbGoodsMapper.selectByExample(example);
        //获得TbGoods的分面数据
        List<TbGoods> tbGoods = pagetbGoods.getResult();
        //转换成Goods类
        List<Goods> goodsList = new ArrayList<>();

        for(int i = 0; i < tbGoods.size(); i++) {
            Goods goods1 = new Goods();
            goods1.setGoods(tbGoods.get(i));
            goodsList.add(goods1);
        }

        //分页返回
        PageResult pageResult = new PageResult(pagetbGoods.getTotal(),goodsList);

        return pageResult;
    }

    @Override
    public Goods findOne(Long id) {
        Goods goods = new Goods();
        TbGoods tbGoods = tbGoodsMapper.selectByPrimaryKey(id);
        goods.setGoods(tbGoods);
        GoodsDesc goodsDesc = goodsDescMapper.selectByPrimaryKey(id);
        goods.setGoodsDesc(goodsDesc);
//        List<Item> items = itemMapper.select
        return goods;
    }
}
