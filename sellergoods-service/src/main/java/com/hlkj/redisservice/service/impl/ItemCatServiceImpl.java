package com.hlkj.redisservice.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.ItemCat;
import com.hlkj.pojo.model.ItemCatExample;
import com.hlkj.pojo.model.ItemCatExample.Criteria;
import com.hlkj.redisservice.mapper.ItemCatMapper;
import com.hlkj.redisservice.service.ItemCatService;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemCatServiceImpl implements ItemCatService
{

    @Autowired
    private ItemCatMapper itemCatMapper;

    @Override
    public List<ItemCat> findAll() {
        return itemCatMapper.selectByExample(null);
    }

    @Override
    public PageResult findPage(Integer pageNo, Integer pageSize) {
        //分页查询空间
        PageHelper.startPage(pageNo, pageSize);
        //分页查询
        Page<ItemCat> page = (Page<ItemCat>)itemCatMapper.selectByExample(null);
        //返回结果转换
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public Result add(ItemCat itemCat) {
        Integer ret = itemCatMapper.insert(itemCat);
        if (ret > 0) {
            return new Result(true, "添加成功");
        } else {
            return new Result(false, "添加失败");
        }
    }

    @Override
    public Result update(ItemCat itemCat) {
        Integer ret=itemCatMapper.updateByPrimaryKey(itemCat);
        if (ret > 0) {
            return new Result(true, "修改成功");
        } else {
            return new Result(false, "修改失败");
        }
    }

    @Override
    public ItemCat findOne(Long id) {
        return itemCatMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult search(Integer pageNo, Integer pageSize, ItemCat itemCat) {
        if (pageNo <= 0){
            pageNo = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }
        //构造查询的example
        ItemCatExample example = new ItemCatExample();
        //构建查询条件
        Criteria criteria = example.createCriteria();

        if(itemCat!=null){

            if(itemCat.getName()!=null&&itemCat.getName()!=""){
                criteria.andNameLike("%" + itemCat.getName() + "%");
            }
            if (itemCat.getParentId()!=null) {
                criteria.andParentIdEqualTo(itemCat.getParentId());
            }
        }

        if(criteria.getAllCriteria().size()>0){
            PageHelper.startPage(pageNo, pageSize);

            Page<ItemCat> page = (Page<ItemCat>)itemCatMapper.selectByExample(example);

            return new PageResult(page.getTotal(), page.getResult());
        } else {
            return findPage(pageNo,pageSize);
        }
    }

    @Autowired
    private SqlSessionFactory sqlSessionFactory;
    @Override
    public Result delete(Long[] ids) {
        if (ids.length > 0) {
            for (int i = 0; i < ids.length; i++) {
                itemCatMapper.deleteByPrimaryKey(ids[i]);
            }
            return new Result(true, "删除成功!");
        } else {
            return new Result(false, "没有要删除的id");
        }
    }

    @Override
    public List<ItemCat> searchItemCatByParentId(Long parentId) {
        ItemCatExample example = new ItemCatExample();
        Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(parentId);
        return itemCatMapper.selectByExample(example);
    }
}
