package com.hlkj.redisservice.service;

import com.hlkj.common.common.PageResult;
import com.hlkj.common.common.Result;
import com.hlkj.pojo.model.ItemCat;

import java.util.List;

public interface ItemCatService {

    List<ItemCat> findAll();

    PageResult findPage(Integer pageNo, Integer pageSize);

    Result add(ItemCat record);

    Result update(ItemCat record);

    ItemCat findOne(Long id);

    PageResult search(Integer pageNo, Integer pageSize, ItemCat record);

    Result delete(Long[] id);

    List<ItemCat> searchItemCatByParentId(Long parentId);
}
