package com.hlkj.redisservice;

import com.hlkj.pojo.model.Brand;
import com.hlkj.redisservice.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class SellergoodsServiceApplicationTests {

    @Autowired
    private BrandService brandService;

    @Test
    void contextLoads() {
        List<Brand> brands = new ArrayList<>();
        Brand brand1 = new Brand();
        brand1.setName("Chao");
        brand1.setFirstChar("C");
        brands.add(brand1);
        brand1 = new Brand();
        brand1.setName("Chao");
        brand1.setFirstChar("C");
        brands.add(brand1);
        brandService.batch(brands);
        System.out.println();
    }

}
