package com.hlkj.pojo.model;

import lombok.Data;

import java.util.List;

@Data
//@NoArgsConstructor
//@AllArgsConstructor
public class Goods {

    private TbGoods goods;

    private GoodsDesc goodsDesc;

    private List<Item> items;

    public Goods() {

    }

    public Goods(TbGoods goods, GoodsDesc goodsDesc, List<Item> items) {
        this.goods = goods;
        this.goodsDesc = goodsDesc;
        this.items = items;
    }
}
