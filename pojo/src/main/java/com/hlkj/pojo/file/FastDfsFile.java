package com.hlkj.pojo.file;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FastDfsFile implements Serializable {
    private Long id;
    private String name;
    private String server;
    private Integer type;
    private String groupname;
    private String path;
    private Date createTime;

    public FastDfsFile(Long id, String name, String server, String groupname, String path, Date createTime) {
        this.id = id;
        this.name = name;
        this.server = server;
        this.groupname = groupname;
        this.path = path;
        this.createTime = createTime;
    }

    public FastDfsFile(String name, String server, String groupname, String path, Date createTime) {
        this.name = name;
        this.server = server;
        this.groupname = groupname;
        this.path = path;
        this.createTime = createTime;
    }
}
